# RCP requirements documentation

This repository holds documents describing the DSA-2000 RCP subsystem requirements.

On gitlab.com a tag is generated for the repository, and rendered
versions of the document are generated, whenever the "release" CI job
is manually started. That job will create a tag based on the last
version number in the list of versions in the `01-frontmatter.md`
file; the purpose being to maintain consistency in document and git
repository version numbers. Multiple rendered versions of the document
are created by the CI pipeline:
* on the
  [wiki](https://gitlab.com/dsa-2000/planning/rcp/docs/req/-/wikis/requirements),
  and
* in a versioned package in the [package
  repository](https://gitlab.com/dsa-2000/planning/rcp/docs/req/-/packages).

## Instructions for generating PDF version of documentation

For those wanting to generate a PDF locally...some examples:

### Interactive version

``` shell
pandoc docs/*.md -o requirements.pdf -s -V colorlinks --template template.latex
```

### Print version

``` shell
pandoc docs/*.md -o requirements.pdf -s -V colorlinks -V links-as-notes --number-sections --template template.latex
```
