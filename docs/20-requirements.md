# RCP requirements

## run-time reliability [R1] {#R1}

Every RCP instance must produce at least X% of its output image streams
over Y% of the time the array is in its observing mode. Note that the
production of images in an output stream is independent of the presence
of valid input data contributing to the image stream.

##### rationale {.unnumbered}

RCP instances must satisfy reliability standards to support DSA-2000
reliability requirements.

##### verification {.unnumbered}

Measurement of expected distribution of output images during times the
array is in an observational mode *vs* actual distribution of output
images in those time intervals.

### failure domains [R1.1] {#R1.1}

Failure domains in an RCP instance should be small.

##### rationale {.unnumbered}

Small failure domains reduce the number of output image streams affected
by a node or process failure.

##### verification {.unnumbered}

Verify the system response when a set of RCP nodes is forcibly shut down
(or similar) during a test observation. System tests may be implemented
for verification.

### failover [R1.2] {#R1.2}

Every node in an RCP instance must be able to host any of the processes
in the RCP instance, subject to RCF-RCP network constraints (if any),
and inter-process constraints in the design of RCP failure domains (if
any).

##### rationale {.unnumbered}

To reduce the time to repair a (partially) failed RCP instance.

##### verification {.unnumbered}

Verify the restoration of RCP functionality after forcibly shutting down
a set of RCP nodes, and starting processes on new nodes to assume the
roles of those that were shut down. System tests may be implemented for
verification.

### stream initialization [R1.3] {#R1.3}

RCP must comply with a protocol between RCP and RCF to direct the RCF
output streams to the nodes that require them at any moment in time (as
determined by the output image streams produced by every node at that
time.)

##### rationale {.unnumbered}

As any image stream in an RCP instance could be produced by any node in
the instance, when RCP processes are (re)started (whether due to
maintenance cycles or failover), the destination of RCF output streams
may need to be redirected to other network addresses.

##### verification {.unnumbered}

Check that RCP input streams are re-established after restarting a set
of RCP processes on an altered node configuration. System tests may be
implemented for verification.

### process control [R1.4] {#R1.4}

The processes comprising an RCP instance must be controllable
individually and collectively through the MNC subsystem on any set of
nodes that are provisioned to support that RCP instance.

##### rationale {.unnumbered}

To minimize the time to repair an RCP instance, as well to reduce the
operational complexity of starting and stopping RCP instances.

##### verification {.unnumbered}

Use MNC interfaces (possibly through OPL?) to start, stop and restart a
set of RCP processes on several different nodes. System tests may be
implemented for verification.

## software reliability [R2] {#R2}

To promote RCP software reliability, the DSA-2000 software practices
standards will be applied.

##### rationale {.unnumbered}

The DSA-2000 software practices standards have been adopted by the
project to promote software quality, including reliability.

##### verification {.unnumbered}

Manual and automated compliance audits.

### CI/CD [R2.1] {#R2.1}

To improve the reliability of RCP, the development, testing and
deployment of RCP software should employ modern CI/CD infrastructure and
methodology. Use of pipelines for rigorous unit and integration tests,
automated to the extent possible given the DSA-2000 facilities and
operational constraints, shall be applied prior to system testing or
deployment of any software component of the RCP.

##### rationale {.unnumbered}

CI practices support proper unit and integration testing of software
prior to release, which improves quality. CD practices assist in the
management of software artifact deployment (including version
rollbacks), and transitions of software through manual system test
stages (*e.g*, test array, full array test, production).

##### verification {.unnumbered}

Manual and automated compliance audits.

### reproducibility [R2.2] {#R2.2}

The RCP software build process, the products of the build process, and
the deployed software artifacts should be reproducible.

##### rationale {.unnumbered}

To improve the reliability of RCP software through development,
debugging, testing, and deployment (in any environment), reproducibility
of the RCP software project build process, its products, and the
installation of artifacts is key.

##### verification {.unnumbered}

Manual compliance audits.

## instance operation [R3] {#R3}

RCP instances should integrate with MNC and OPL subsystems.

##### rationale {.unnumbered}

DSA-2000 requires a number of RCP instances integrated into the
operational system.

##### verification {.unnumbered}

All RCP instances can be fully operated and monitored using standard
DSA-2000 subsystems. Integration tests may be implemented for
verification.

### logging [R3.1] {#R3.1}

RCP instances shall integrate with the logging infrastructure of the
system.

##### rationale {.unnumbered}

Logging provides valuable historical information about the run time
behavior of RCP instances, which can be used for various diagnostic
purposes.

##### verification {.unnumbered}

All RCP instances write logs to the DSA-2000 logging facility according
to their build-time and run-time configurations. Integration tests may
be implemented for verification.

#### logging identification [R3.1.1] {#R3.1.1}

Log messages shall be identified with both logical and physical process
parameters.

##### rationale {.unnumbered}

Identification of the source of log messages may require logical and/or
physical identifiers for efficient access, depending on the use case.

##### verification {.unnumbered}

Unit tests may be implemented for verification.

### monitor [R3.2] {#R3.2}

RCP instances shall provide monitor capabilities that integrate with the
MNC subsystem.

##### rationale {.unnumbered}

To facilitate understanding and diagnostics of the run time state of RCP
instances in the DSA-2000.

##### verification {.unnumbered}

Integration tests may be implemented for verification.

#### monitor point identification [R3.2.1] {#R3.2.1}

Monitor points shall be identified with both logical and physical
process parameters.

##### rationale {.unnumbered}

Identification of monitor points may require logical and/or physical
identifiers for efficient access, depending on the use case.

##### verification {.unnumbered}

Unit tests may be implemented for verification.

### control [R3.3] {#R3.3}

RCP instances shall provide control capabilities that integrate with the
MNC subsystem.

##### rationale {.unnumbered}

To facilitate operation of DSA-2000 RCP instances.

##### verification {.unnumbered}

Integration tests may be implemented for verification.

#### control point identification [R3.3.1] {#R3.3.1}

Control points shall be identified with both logical and physical
process parameters.

##### rationale {.unnumbered}

Identification of control points may require logical and/or physical
identifiers for efficient access, depending on the use case.

##### verification {.unnumbered}

Unit tests may be implemented for verification.

### configuration [R3.4] {#R3.4}

RCP instances shall be configurable upon startup for scalability,
testability, and usability on any set of appropriately provisioned
nodes.

##### rationale {.unnumbered}

As the complete DSA-2000 RCP subsystem comprises several "instances" of
imaging pipelines -- for example, imaging pipelines for polarimetric
images, and zoom band A images -- to promote consistency in the
deployment and operation of all instances, the same software should be
used for all of them, and thus that software must be highly
configurable. Scalability, testability and usability are all needed to
promote software development and testing, as well as production
deployment.

##### verification {.unnumbered}

Compliance audit that a single software code is used for all imaging
pipeline instances in DSA-2000. Deployment of instances should differ
only in values of well-known configuration points exposed by the
software build system or by its executable artifacts at run-time.

### deployment [R3.5] {#R3.5}

RCP instances should be deployable through an interface accessible to
the MNC subsystem.

##### rationale {.unnumbered}

Deployment of RCP instances in DSA-2000 should integrate with the
capabilities of the MNC subsystem to promote usability and reliability.

##### verification {.unnumbered}

System tests may be used for verification that RCP instances are fully
deployable throughout DSA-2000 using MNC subsystem capabilities. Every
RCP instance must be independently deployable, without interference to
other instances.

### modes [R3.6] {#R3.6}

Modes are defined by the production of data products in a designated
output stream of an RCP instance. For example, the "imaging mode" is
active whenever images are being produced; the "gain calibration" mode
is active whenever gain calibration solutions are being computed;
*etc.* Modes are not mutually exclusive, and may be active
concurrently.

The conditions that control the modes of an RCP instance should result
from signals originating from external sources, or be internally
deterministically triggered based on prior received signals according to
a publicly specified protocol.

##### rationale {.unnumbered}

External control of RCP data product streams, together with internal,
deterministic state changes allows the state of data product streams to
be known externally without querying the RCP instance. This promotes
deterministic mode changes in RCP instances.

##### verification {.unnumbered}

Unit and integration tests may be used for verification.

#### one-shot modes [R3.6.1] {#R3.6.1}

RCP modes shall persist for a single instance of all output products on
the stream(s) affected by a start condition.

Note that computing and exporting calibration solutions every
integration doesn't fit this model well, but then, neither does the
model of exporting solutions at such a high rate for use by other RCP
instances. Relaxing this requirement for that case could be made, if it
becomes necessary.

##### rationale {.unnumbered}

Simplifies the implementation of mode stop conditions, and is applicable
to all known use cases for RCP data products (*e.g*, image one field,
slew to next field, image one field, ...).

##### verification {.unnumbered}

Unit tests may be used for verification.

### time reference [R3.7] {#R3.7}

Time values used by external interfaces to RCP should be referenced to
timestamp values in the RCP input data streams whenever
possible. Times values should generally not be referenced to the
computer system clocks in RCP, except when input data streams are
absent or time values are specifically identified as not referencing
timestamps in input data streams.

When input data streams are absent, time values that would normally
reference timestamps in the input data streams may be referenced to a
computer system clock as long as the input data streams remain
absent. It is recommended that in such cases the computer system
clock reference be adjusted backward by a margin moderately larger
than the maximum expected system clock difference across DSA-2000.

##### rationale {.unnumbered}

Reduce the need in RCP to account for data latency or discrepancies of
different clocks in DSA-2000.

##### verification {.unnumbered}

Inspection of RCP software source code.

#### time reference base monitor point [R3.7.1] {#R3.7.1}

RCP shall implement a monitor point that indicates the current default
time reference base, either the RCP input data streams or the system
clock.

##### rationale {.unnumbered}

For diagnostic purposes, to aid in understanding how time references
in RCP external interfaces are resolved in an RCP imaging
pipeline. Secondarily, this monitor point provides an indication of
the presence or absence of input data streams.

##### verification {.unnumbered}

Test that the monitor point value changes as expected when an input
data stream is alternately (externally) switched on and off.

## instance output image streams [R4] {#R4}

Images in all RCP instance output image streams shall be produced with
the following characteristics:

- one every 10.3 minutes
- single precision floating point values
- image size of 16k x 16k pixels
- pixel size inversely proportional to frequency, with 0.5" size at 1.4
  GHz

##### rationale {.unnumbered}

These image parameters satisfy the science requirements (TODO: ref?),
and the DAT subsystem.

##### verification {.unnumbered}

Unit tests may be used for verification.

### image handoff to DAT [R4.1] {#R4.1}

As no fixed association of image streams to RCP instances is required,
the protocol implemented for the transfer of RCP images to the DAT
subsystem must provide sufficient information for DAT to locate the file
containing an image.

##### rationale {.unnumbered}

The DAT subsystem is the consumer of image products from RCP; a loose
coupling of RCP and DAT via this interface promotes system reliability,
as well as software subsystem independence.

##### verification {.unnumbered}

Integration tests may be used for verification.

## RCP-H instance [R5] {#R5}

An instance of RCP shall be deployed that produces the Medium
Polarimetric (MP) images above 1.45 GHz.

##### rationale {.unnumbered}

Processing of the high end of the DSA-2000 band at the NC channel
resolution requires a configured and deployed instance of RCP
specifically for the image products required in that frequency range.

##### verification {.unnumbered}

System tests may be used for verification.

### RCP-H calibration [R5.1] {#R5.1}

RCP-H shall compute calibration solutions in the upper part of the
DSA-2000 band, as needed, and, if needed, also export those solutions
for availability across the system.

##### rationale {.unnumbered}

Within RCP generally, RCP-H is used to compute calibration solutions in
the upper part of the full DSA-2000 band. These solutions are used
internally by RCP-H, and potentially for other uses in the system.

##### verification {.unnumbered}

Unit tests may be used for verification.

#### calibration solution quality metrics [R5.1.1] {#R5.1.1}

Quality metrics for computed calibration solutions TBD, as required by
their use within RCP-H, and any other uses (TBD, if they exist).

##### rationale {.unnumbered}

This requirement reflects the most stringent required quality of
calibration solutions for all uses in DSA-2000, including RCP-H itself.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

### RCP-H input streams [R5.2] {#R5.2}

RCP-H shall receive all RCF NC channel data above 1.45 GHz.

##### rationale {.unnumbered}

RCP-H is deployed for that part of the full band over which only MP
images are produced, which implies it must receive the NC channel
resolution voltage data within that part of the band.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

### RCP-H output streams [R5.3] {#R5.3}

RCP-H shall produce required outputs above 1.45 GHz.

##### rationale {.unnumbered}

All data products (not only images) that depend on the channelized data
in the RCP-H input streams must be produced by RCP-H.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

#### image streams [R5.3.1] {#R5.3.1}

RCP-H shall produce the following image streams:

- MP images

##### rationale {.unnumbered}

These images are dependent upon the channelized voltage data received by
RCP-H.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

#### calibration solution streams [R5.3.2] {#R5.3.2}

RCP-H shall produce a streams of calibration solutions in the part of
the band covered by its input streams.

##### rationale {.unnumbered}

Calibration solutions may be needed by RCF in this part of the band.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

### RCP-H imaging pipeline [R5.4] {#R5.4}

RCP-H shall implement an imaging pipeline for its input streams with the
following stages, coarsely defined:

- cross-correlation
- flagging
- calibration solution
- calibration application
- gridding
- Fourier transform

##### rationale {.unnumbered}

Creating calibrated images from channelized voltage data received by
RCP-H requires a pipeline with these steps.

##### verification {.unnumbered}

Unit tests may be used for verification.

#### correlation interval [R5.4.1] {#R5.4.1}

The correlation interval shall not exceed 1.5 seconds.

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

#### flagging quality metrics [R5.4.2] {#R5.4.2}

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

#### calibration quality metrics [R5.4.3] {#R5.4.3}

Requirements for calibration quality for RCP-H imaging.

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

#### gridding quality metrics [R5.4.4] {#R5.4.4}

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

### RCP-H node provisioning [R5.5] {#R5.5}

Nodes in RCP-H shall be provisioned to support the following
functionality:

- imaging pipelines required by the distribution of RCF output streams
  to the node (*i.e*, the node input streams)
- DAT functionality required for output streams (refers to DAT processes
  executed on RCP-H nodes)

##### rationale {.unnumbered}

Nodes must be provisioned with sufficient resources to provide all
services required of a node in RCP-H.

##### verification {.unnumbered}

System tests may be used for verification that nodes in RCP-H have
access to the resources needed to function effectively.

#### Network interfaces [R5.5.1] {#R5.5.1}

RCP-H nodes shall be provisioned with network interfaces on two
networks: one network for RCP input streams and calibration solutions
exported to RCF, and another network for other IPL, and DAT use. For
RCF network, 2 100Gb Ethernet; for IPL and DAT network, 1 100Gb
Ethernet.

##### rationale {.unnumbered}

Every sixteen RCF NC channels require 68 Gbps of RCP input network
bandwidth. Assuming 32 channels, two 100 GbE NICs are sufficient.

IPL/DAT network is primarily determined by requirements of shared file
system used by DAT. Inter-node IPL network traffic is minimal or
non-existent.

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with sixteen
or thirty-two input channels.

#### CPU parameters [R5.5.2] {#R5.5.2}

At least six CPU cores per GPU. With eight GPUs per node, 48 CPU
cores. Performance equivalent or better than 4th generation Intel Xeon
or AMD Epyc.

##### rationale {.unnumbered}

Estimate of CPU core usage by IPL, including calibration.

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with sixteen
or thirty-two input channels.

#### system RAM [R5.5.3] {#R5.5.3}

64 GiB RAM per GPU. With eight GPUs per node, 512 GiB.

##### rationale {.unnumbered}

512 GiB is sufficient for IPL;, requiring data capture buffers,
voltage samples, some number of times (from pending input arrays to
cross-correlation), a subset of convolution function kernels (?), full
grids, calibration requirements

| Samples                     |        |
|-----------------------------+--------|
| sample size                 |      1 |
| num receivers               |   2000 |
| samples per vis             | 200000 |
| total per Tv per node (GiB) |     12 |

| Grids                |        |
|----------------------+--------|
| MP img per node      |      4 |
| fp size              |      8 |
| total per node (GiB) |     16 |

Calibration: 256 GiB

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with sixteen
or thirty-two input channels.

#### system disk [R5.5.4] {#R5.5.4}

4 TiB (usable) per GPU, with RAID. With eight GPUs per node, 32 TiB.

##### rationale {.unnumbered}

##### verification {.unnumbered}

#### GPU parameters [R5.5.5] {#R5.5.5}

An integer multiple of four GPUs, each with at least 16 GB
memory. Assuming 32 input channels, eight GPUs are
required. Performance equivalent or better than NVIDIA RTX 4000 SFF
Ada generation.

##### rationale {.unnumbered}

Four RCF NC channels are processed per GPU, and sixteen NC channels
are required for an MP image channel. Spreading NC channels that are
averaged into a single image channel across multiple nodes should be
avoided, as it increases inter-node communication requirements.

16 GB memory per GPU is sufficient for IPL, requiring grid tiles,
sample and visibility buffers. Assume double buffering for all data
regions.

| Grid tiles          |        |
|---------------------+---------|
| cell size           |     1.0 |
| central tile size   |    6827 |
| MP img per gpu      |       4 |
| fp size             |       8 |
| total per gpu (GiB) |       3 |

| Samples                    |        |
|----------------------------+--------|
| sample size                |      1 |
| num receivers              |   2000 |
| samples per vis            | 200000 |
| total per Tv per gpu (GiB) |      3 |

| Visibilities               |        |
|----------------------------+--------|
| fp size                    |      4 |
| num baselines              |    2e6 |
| chan per gpu               |      4 |
| total per Tv per gpu (GiB) |   0.25 |

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with sixteen
or thirty-two input channels.

#### shared file system access [R5.5.6] {#R5.5.6}

RCP-H nodes require access to the DAT shared file system.

##### rationale {.unnumbered}

DAT processing writes to a shared file system.

##### verification {.unnumbered}

## RCP-L instance [R6] {#R6}

An instance of RCP shall be deployed that produces the Medium
Polarimetric (MP) and Narrow Line (NL) images below 1.45 GHz.

##### rationale {.unnumbered}

Processing of the low end of the DSA-2000 band at the NC channel
resolution requires a configured and deployed instance of RCP
specifically for the image products required in that frequency range.

##### verification {.unnumbered}

System tests may be used for verification.


### RCP-L calibration [R6.1] {#R6.1}

RCP-L shall compute calibration solutions in the lower part of the
DSA-2000 band, as needed, and also export those solutions for
availability across the system (for example, by RCP-A and RCP-B).

##### rationale {.unnumbered}

Within RCP generally, RCP-L is used to compute calibration solutions in
the lower part of the full DSA-2000 band. These solutions are used
internally by RCP-H, and for other uses in the system, including RCP-A
and RCP-B.

##### verification {.unnumbered}

Unit tests may be used for verification.

#### calibration solution quality metrics [R6.1.1] {#R6.1.1}

Quality metrics for computed calibration solutions TBD, as required by
their use within RCP-L, and any other uses, including RCP-A and RCP-B.

##### rationale {.unnumbered}

This requirement reflects the most stringent required quality of
calibration solutions for all uses in DSA-2000, including RCP-L itself,
RCP-A and RCP-B.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

### RCP-L input streams [R6.2] {#R6.2}

RCP-L shall receive  RCF NC channel data below 1.45 GHz.

##### rationale {.unnumbered}

RCP-L is deployed for that part of the full band over which both MP and
NL images are produced, which implies it must receive the NC channel
resolution voltage data within that part of the band.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

### RCP-L output streams [R6.3] {#R6.3}

RCP-L shall produce required outputs below 1.45 GHz.

##### rationale {.unnumbered}

All data products (not only images) that depend on the channelized data
in the RCP-L input streams must be produced by RCP-L.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

#### image streams [R6.3.1] {#R6.3.1}

RCP-L shall produce the following image streams:

- MP images
- NL images

##### rationale {.unnumbered}

These images are dependent upon the channelized voltage data received by
RCP-L.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

#### calibration solution streams [R6.3.2] {#R6.3.2}

RCP-L shall produce a streams of calibration solutions in the part of
the band covered by its input streams.

##### rationale {.unnumbered}

Calibration solutions may be needed by RCF, RCP-A and RCP-B in this part
of the band.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

### RCP-L imaging pipeline [R6.4] {#R6.4}

RCP-L shall implement an imaging pipeline for its input stream with the
following stages, coarsely defined:

- cross-correlation
- flagging
- calibration solution
- calibration application
- gridding
- Fourier transform

##### rationale {.unnumbered}

Creating calibrated images from channelized voltage data received by
RCP-L requires a pipeline with these steps.

##### verification {.unnumbered}

Unit tests may be used for verification.

#### correlation interval [R6.4.1] {#R6.4.1}

The correlation interval shall not exceed 1.5 seconds.

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

#### flagging quality metrics [R6.4.2] {#R6.4.2}

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

#### calibration quality metrics [R6.4.3] {#R6.4.3}

Requirements for calibration quality for RCP-L imaging.

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

#### gridding quality metrics [R6.4.4] {#R6.4.4}

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

### RCP-L node provisioning [R6.5] {#R6.5}

Nodes in RCP-L shall be provisioned to support the following
functionality:

- imaging pipelines required by the distribution of RCF output streams
  to the node (*i.e*, the node input streams)
- DAT functionality required for output streams (refers to DAT processes
  executed on RCP-F nodes)

##### rationale {.unnumbered}

Nodes must be provisioned with sufficient resources to provide all
services required of a node in RCP-L.

##### verification {.unnumbered}

System tests may be used for verification that nodes in RCP-L have
access to the resources needed to function effectively.

#### Network interfaces [R6.5.1] {#R6.5.1}

RCP-L nodes shall be provisioned with network interfaces on two
networks: one network for RCP input streams and calibration solutions
exported to RCF, and another network for other IPL, and DAT use. For
RCF network, 2 100Gb Ethernet; for IPL and DAT network, 1 100Gb
Ethernet.

##### rationale {.unnumbered}

Every sixteen RCF NC channels require 68 Gbps of RCP input network
bandwidth. Assuming 32 channels, two 100 GbE NICs are sufficient.

IPL/DAT network is primarily determined by requirements of shared file
system used by DAT. This network is also used for export of calibration
solutions to RCP-A and RCP-B.

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with sixteen
or thirty-two input channels.

#### CPU parameters [R6.5.2] {#R6.5.2}

At least six CPU cores per GPU. With eight GPUs per node, 48 CPU
cores. Performance equivalent or better than 4th generation Intel Xeon
or AMD Epyc.

##### rationale {.unnumbered}

Estimate of CPU core usage by IPL, including calibration.

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with sixteen
or thirty-two input channels.

#### system RAM [R6.5.3] {#R6.5.3}

64 GiB RAM per GPU. With eight GPUs per node, 512 GiB.

##### rationale {.unnumbered}

512 GiB is sufficient for IPL;, requiring data capture buffers,
voltage samples, some number of times (from pending input arrays to
cross-correlation), a subset of convolution function kernels (?), full
grids, calibration requirements

| Samples                     |        |
|-----------------------------+--------|
| sample size                 |      1 |
| num receivers               |   2000 |
| samples per vis             | 200000 |
| total per Tv per node (GiB) |     12 |

| Grids                |        |
|----------------------+--------|
| MP img per node      |      4 |
| NL img per node      |     16 |
| fp size              |      8 |
| total per node (GiB) |     80 |

Calibration: 256 GiB

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with sixteen
or thirty-two input channels.

#### system disk [R6.5.4] {#R6.5.4}

14 TiB (usable) per GPU, with RAID. With eight GPUs per node, 112 TiB.

##### rationale {.unnumbered}

##### verification {.unnumbered}

#### GPU parameters (inc. memory) [R6.5.5] {#R6.5.5}

An integer multiple of four GPUs, each with at least 20 GB
memory. Assuming 32 input channels, eight GPUs are
required. Performance equivalent or better than NVIDIA RTX 4000 SFF
Ada generation.

##### rationale {.unnumbered}

Four RCF NC channels are processed per GPU, and sixteen NC channels
are required for an MP image channel. Spreading NC channels that are
averaged into a single image channel across multiple nodes should be
avoided, as it increases inter-node communication requirements.

20 GB memory per GPU is sufficient for IPL, requiring grid tiles,
sample and visibility buffers. Assume double buffering for all data
regions.

| Grid tiles          |        |
|---------------------+--------|
| cell size           |    1.0 |
| central tile size   |   6827 |
| MP img per gpu      |      4 |
| NL img per gpu      |      4 |
| fp size             |      8 |
| total per gpu (GiB) |      6 |

| Samples                    |        |
|----------------------------+--------|
| sample size                |      1 |
| num receivers              |   2000 |
| samples per vis            | 200000 |
| total per Tv per gpu (GiB) |      3 |

| Visibilities               |         |
|----------------------------+---------|
| fp size                    |       4 |
| num baselines              |     2e6 |
| chan per gpu               |       4 |
| total per Tv per gpu (GiB) |    0.25 |

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with sixteen
or thirty-two input channels.

#### shared file system access [R6.5.6] {#R6.5.6}

RCP-L nodes require access to the DAT shared file system.

##### rationale {.unnumbered}

DAT processing writes to a shared file system.

##### verification {.unnumbered}

## RCP-A instance [R7] {#R7}

An instance of RCP shall be deployed that produces the Zoom A (ZA)
images.

##### rationale {.unnumbered}

Processing of DSA-2000 Zoom A band at the CA channel resolution requires
a configured and deployed instance of RCP specifically for the image
products required by Zoom A.

##### verification {.unnumbered}

System tests may be used for verification.

### RCP-A input streams [R7.1] {#R7.1}

RCP-A shall receive all RCF AC channel data.

##### rationale {.unnumbered}

RCP-A is deployed for the Zoom A band, which implies it must receive all
input streams required to produce the images in that band.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

#### RCP-A voltage data input [R7.1.1] {#R7.1.1}

RCP-A shall receive all channelized data from RCF with CA channel
frequency resolution.

##### rationale {.unnumbered}

RCP-A is deployed for the Zoom A band, which implies it must receive the
CA channel resolution voltage data within that band.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

#### RCP-A calibration solutions input [R7.1.2] {#R7.1.2}

RCP-A requires calibration solutions that have been externally computed.

##### rationale {.unnumbered}

RCP-A is not intended to produce its own calibration solutions, rather
it will acquire them from an external source as needed, to produce
calibrated images.

##### verification {.unnumbered}

Unit, integration and system tests may be used for verification.

### RCP-A output image streams [R7.2] {#R7.2}

RCP-A shall produce the following image streams:

- ZA images

##### rationale {.unnumbered}

These images are dependent upon the channelized voltage data received by
RCP-A.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

### RCP-A imaging pipeline [R7.3] {#R7.3}

RCP-A shall implement an imaging pipeline for its input stream with the
following stages, coarsely defined:

- cross-correlation
- flagging
- calibration application
- gridding
- Fourier transform

##### rationale {.unnumbered}

Creating calibrated images from channelized voltage data received by
RCP-A requires a pipeline with these steps.

##### verification {.unnumbered}

Unit tests may be used for verification.

#### correlation interval [R7.3.1] {#R7.3.1}

The correlation interval shall not exceed 1.5 seconds.

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

#### flagging quality metrics [R7.3.2] {#R7.3.2}

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

#### calibration quality metrics [R7.3.3] {#R7.3.3}

Requirements for calibration quality for RCP-A imaging.

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

#### gridding quality metrics [R7.3.4] {#R7.3.4}

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

### RCP-A node provisioning [R7.4] {#R7.4}

Nodes in RCP-A shall be provisioned to support the following
functionality:

- imaging pipelines required by the distribution of RCF output streams
  to the node (*i.e*, the node input streams)
- DAT functionality required for node output streams (refers to DAT
  processes executed on RCP-A nodes)

##### rationale {.unnumbered}

Nodes must be provisioned with sufficient resources to provide all
services required of a node in RCP-A.

##### verification {.unnumbered}

System tests may be used for verification that nodes in RCP-A have
access to the resources needed to function effectively.

#### Network interfaces [R7.4.1] {#R7.4.1}

RCP-A nodes shall be provisioned with network interfaces on two
networks: one network for RCP input streams, and another network for
other IPL, and DAT use. For RCF network, 1 100Gb Ethernet; for IPL and
DAT network, 1 100Gb Ethernet.

##### rationale {.unnumbered}

Forty RCF AC channels requires 11 Gbps of RCP input network bandwidth.

At a minimum, the IPL/DAT network is used for importing calibration
solutions from RCP-L.

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with forty input
channels.

#### CPU parameters [R7.4.2] {#R7.4.2}

At least four CPU cores per GPU. With ten GPUs per node, 40 CPU
cores. Performance equivalent or better than 4th generation Intel Xeon
or AMD Epyc.

##### rationale {.unnumbered}

Estimate of CPU core usage by IPL.

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with forty input
channels.

#### system RAM [R7.4.3] {#R7.4.3}

32 GiB RAM per GPU. With ten GPUs per node, 320 GiB.

##### rationale {.unnumbered}

320 GiB is sufficient for IPL;, requiring data capture buffers,
voltage samples, some number of times (from pending input arrays to
cross-correlation), a subset of convolution function kernels (?), full
grids.

| Samples                     |           |
|-----------------------------+-----------|
| sample size                 |         1 |
| num receivers               |      2000 |
| samples per vis             | 12195.122 |
| total per Tv per node (GiB) |         2 |

| Grids                |     |
|----------------------+-----|
| ZA img per node      |  40 |
| fp size              |   8 |
| total per node (GiB) | 160 |

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with forty input
channels.

#### system disk [R7.4.4] {#R7.4.4}

12 TiB (usable) per GPU, with RAID. With ten GPUs per node, 120 TiB.

##### rationale {.unnumbered}

##### verification {.unnumbered}

#### GPU parameters (inc. memory) [R7.4.5] {#R7.4.5}

Any number of GPUs, each with at least 8 GB memory. Assuming 40 input
channels, ten GPUs are required. Performance equivalent or better
than NVIDIA RTX 4000 SFF Ada generation.

##### rationale {.unnumbered}

8 GB memory per GPU is sufficient for IPL, requiring grid tiles,
sample and visibility buffers. Assume double buffering for all data
regions.

| Grid tiles          |      |
|---------------------+------|
| cell size           |  1.0 |
| central tile size   | 6827 |
| img per gpu         |    4 |
| fp size             |    8 |
| total per gpu (GiB) |    3 |

| Samples                    |           |
|----------------------------+-----------|
| sample size                |         1 |
| num receivers              |      2000 |
| samples per vis            | 12195.122 |
| total per Tv per gpu (GiB) |       0.2 |

| Visibilities               |      |
|----------------------------+------|
| fp size                    |    4 |
| num baselines              |  2e6 |
| chan per gpu               |    4 |
|----------------------------+------|
| total per Tv per gpu (GiB) | 0.25 |

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with forty input
channels.

#### shared file system access [R7.4.6] {#R7.4.6}

RCP-A nodes require access to the DAT shared file system.

##### rationale {.unnumbered}

DAT processing writes to a shared file system.

##### verification {.unnumbered}

## RCP-B instance [R8] {#R8}

An instance of RCP shall be deployed that produces the Zoom B (ZB)
images.

##### rationale {.unnumbered}

Processing of DSA-2000 Zoom B band at the CB channel resolution requires
a configured and deployed instance of RCP specifically for the image
products required by Zoom B.

##### verification {.unnumbered}

System tests may be used for verification.

### RCP-B input streams [R8.1] {#R8.1}

RCP-B shall receive all RCF BC channel data.

##### rationale {.unnumbered}

RCP-B is deployed for the Zoom B band, which implies it must receive all
input streams required to produce the images in that band.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

#### RCP-B voltage data input [R8.1.1] {#R8.1.1}

RCP-B shall receive all channelized data from RCF with CB channel
frequency resolution.

##### rationale {.unnumbered}

RCP-B is deployed for the Zoom B band, which implies it must receive the
CB channel resolution voltage data within that band.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

#### RCP-B calibration solutions input [R8.1.2] {#R8.1.2}

RCP-B requires calibration solutions that have been externally computed.

##### rationale {.unnumbered}

RCP-B is not intended to produce its own calibration solutions, rather
it will acquire them from an external source as needed, to produce
calibrated images.

##### verification {.unnumbered}

Unit, integration and system tests may be used for verification.

### RCP-B output image streams [R8.2] {#R8.2}

RCP-B shall produce the following image streams:

- ZB images

##### rationale {.unnumbered}

These images are dependent upon the channelized voltage data received by
RCP-B.

##### verification {.unnumbered}

Unit and system tests may be used for verification.

### RCP-B imaging pipeline [R8.3] {#R8.3}

RCP-B shall implement an imaging pipeline for its input stream with the
following stages, coarsely defined:

- cross-correlation
- flagging
- calibration application
- gridding
- Fourier transform

##### rationale {.unnumbered}

Creating calibrated images from channelized voltage data received by
RCP-B requires a pipeline with these steps.

##### verification {.unnumbered}

Unit tests may be used for verification.

#### correlation interval [R8.3.1] {#R8.3.1}

The correlation interval shall not exceed 1.5 seconds.

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

#### flagging quality metrics [R8.3.2] {#R8.3.2}

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

#### calibration quality metrics [R8.3.3] {#R8.3.3}

Requirements for calibration quality for RCP-B imaging.

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

#### gridding quality metrics [R8.3.4] {#R8.3.4}

##### rationale {.unnumbered}

##### verification {.unnumbered}

Unit tests may be used for verification.

### RCP-B node provisioning [R8.4] {#R8.4}

Nodes in RCP-B shall be provisioned to support the following
functionality:

- imaging pipelines required by the distribution of RCF output streams
  to the node (*i.e*, the node input streams)
- DAT functionality required for node output streams (refers to DAT
  processes executed on RCP-B nodes)

##### rationale {.unnumbered}

Nodes must be provisioned with sufficient resources to provide all
services required of a node in RCP-B.

##### verification {.unnumbered}

System tests may be used for verification that nodes in RCP-B have
access to the resources needed to function effectively.

#### Network interfaces [R8.4.1] {#R8.4.1}

RCP-B nodes shall be provisioned with network interfaces on two
networks: one network for RCP input streams, and another network for
other IPL, and DAT use. For RCF network, 1 100Gb Ethernet; for IPL and
DAT network, 1 100Gb Ethernet.

##### rationale {.unnumbered}

Forty RCF BC channels requires 2 Gbps of RCP input network bandwidth.

At a minimum, the IPL/DAT network is used for importing calibration
solutions from RCP-L.

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with forty input
channels.

#### CPU parameters [R8.4.2] {#R8.4.2}

At least four CPU cores per GPU. With ten GPUs per node, 40 CPU
cores. Performance equivalent or better than 4th generation Intel Xeon
or AMD Epyc.

##### rationale {.unnumbered}

Estimate of CPU core usage by IPL.

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with forty input
channels.

#### system RAM [R8.4.3] {#R8.4.3}

32 GiB RAM per GPU. With ten GPUs per node, 320 GiB.

##### rationale {.unnumbered}

320 GiB is sufficient for IPL;, requiring data capture buffers,
voltage samples, some number of times (from pending input arrays to
cross-correlation), a subset of convolution function kernels (?), full
grids.

| Samples                     |           |
|-----------------------------+-----------|
| sample size                 |         1 |
| num receivers               |      2000 |
| samples per vis             | 1525.9410 |
| total per Tv per node (GiB) |      0.25 |

| Grids                |     |
|----------------------+-----|
| img per node         |  40 |
| fp size              |   8 |
| total per node (GiB) | 160 |

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with forty input
channels.

#### system disk [R8.4.4] {#R8.4.4}

12 TiB (usable) per GPU, with RAID. With ten GPUs per node, 120 TiB.

##### rationale {.unnumbered}

##### verification {.unnumbered}

#### GPU parameters (inc. memory) [R8.4.5] {#R8.4.5}

Any number of GPUs, each with at least 8 GB memory. Assuming 40 input
channels, ten GPUs are required. Performance equivalent or better
than NVIDIA RTX 4000 SFF Ada generation.

##### rationale {.unnumbered}

8 GB memory per GPU is sufficient for IPL, requiring grid tiles,
sample and visibility buffers. Assume double buffering for all data
regions.

| Grid tiles          |       |
|---------------------+-------|
| cell size           |   1.0 |
| central tile size   |  6827 |
| img per gpu         |     4 |
| fp size             |     8 |
| total per gpu (GiB) |     3 |

| Samples                    |           |
|----------------------------+-----------|
| sample size                |         1 |
| num receivers              |      2000 |
| samples per vis            | 1525.9410 |
| total per Tv per gpu (GiB) |      0.05 |

| Visibilities               |      |
|----------------------------+------|
| fp size                    |    4 |
| num baselines              |  2e6 |
| chan per gpu               |    4 |
| total per Tv per gpu (GiB) | 0.25 |

##### verification {.unnumbered}

Verify performance on a fully provisioned single node with forty input
channels.

#### shared file system access [R8.4.6] {#R8.4.6}

RCP-B nodes require access to the DAT shared file system.

##### rationale {.unnumbered}

DAT processing writes to a shared file system.

##### verification {.unnumbered}
