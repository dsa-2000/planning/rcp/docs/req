# Introduction

Level 2 requirements, and below, for the DSA-2000 Radio Camera
Processor subsystem. Requirements for fast time domain and pulsar
timing subsystem components may be found in other documents.
